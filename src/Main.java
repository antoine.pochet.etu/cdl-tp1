import org.apache.commons.io.FilenameUtils;
import org.ini4j.Wini;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Map;
import java.util.Properties;

public class Main {

    private static final String FICHIER_DE_CONFIGURATION = "file/configuration.xml";

    public static void main(String[] args) {
        String pathFile;
        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("--configuration")) {
                    pathFile = args[i + 1];
                }
            }
            pathFile = args[1];
        } else if (System.getenv("FICHIER_DE_CONFIGURATION") != null) {
            pathFile = System.getenv("FICHIER_DE_CONFIGURATION");
        } else {
            pathFile = FICHIER_DE_CONFIGURATION;
        }

        File file = new File(pathFile);
        String extension = FilenameUtils.getExtension(pathFile);

        switch (extension) {
            case "xml":
                try {
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document document = db.parse(file);
                    document.getDocumentElement().normalize();

                    printNodeList(document.getChildNodes());

                } catch (IOException | SAXException | ParserConfigurationException e) {
                    e.printStackTrace();
                }
                break;

            case "json":
                JSONParser jsonParser = new JSONParser();
                try {
                    JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader(file));

                    for (Object key : jsonObject.keySet()) {
                        System.out.println(key + " = " + jsonObject.get(key));
                    }
                } catch (ParseException | IOException e) {
                    e.printStackTrace();
                }
                break;

            case "yaml":
                try {
                    InputStream inputStream = new FileInputStream(file);

                    Yaml yaml = new Yaml();
                    Map<String, Object> data = yaml.load(inputStream);
                    System.out.println(data);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            case "ini":
                try {
                    Wini ini = new Wini(file);

                    for (String key : ini.keySet()) {
                        System.out.println(key + " = " + ini.get(key));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case "properties":
                try (FileInputStream input = new FileInputStream(file)) {
                    PrintWriter writer = new PrintWriter(System.out);

                    Properties properties = new Properties();
                    properties.load(input);

                    properties.list(writer);
                    writer.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            default:
                System.out.println("Extension " + extension + " pas prise en compte par le logiciel");
                break;

        }
    }

    private static void printNodeList(NodeList nodeList) {
        for (int count = 0; count < nodeList.getLength(); count++) {
            Node elemNode = nodeList.item(count);
            if (elemNode.getNodeType() == Node.ELEMENT_NODE) {
                System.out.println("Node " + elemNode.getNodeName() + " = " + elemNode.getTextContent());
                if (elemNode.hasAttributes()) {
                    NamedNodeMap nodeMap = elemNode.getAttributes();
                    for (int i = 0; i < nodeMap.getLength(); i++) {
                        Node node = nodeMap.item(i);
                        System.out.println(node.getNodeName() + " = " +  node.getNodeValue());
                    }
                }
                if (elemNode.hasChildNodes()) {
                    printNodeList(elemNode.getChildNodes());
                }
            }
        }
    }
}